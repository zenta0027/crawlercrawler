﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Spawn a random GameObject from a spawn pool.
 * It can be set to only have a chance to spawn
 */
public class GameObjectSpawner : SpawnChance {

    //Objects it can spawn
    public GameObject[] SpawnPool;
    public int count = 1;
    public int LevelOffset = 0;
    protected override void Awake()
    {        
        //base.Awake();
        //Only spawn the object if the chance to spawn is valid
        for (int i =0; i < count; i++)
        {
            isSpawned = Random.Range(0, 1f) < chanceTospawn;
            if (isSpawned)
            {
                GameObject chosen = SpawnPool[Random.Range(0, SpawnPool.Length)];
                GameObject inst = Instantiate(chosen, transform.position + new Vector3(Random.Range(-2f, 2f), 0f, Random.Range(-2f, 2f)), chosen.transform.rotation, transform.parent);
                if (inst.GetComponent<EnemyObj>() != null)
                {
                    inst.GetComponent<EnemyObj>().SetLevel(LevelOffset);
                }
                //Instantiate(chosen, transform.position, chosen.transform.rotation, transform.parent);
                
            }
        }
    }
}
